__UPDATE: According to npm Inc., the issue has been resolved.__ This gist will be updated with a link to the post-mortem once it's published. Please make sure to read the below explanation anyway, since there may still be a security impact for you.

__UPDATE 2:__ An [initial statement](http://blog.npmjs.org/post/169432444640/npm-operational-incident-6-jan-2018) has been released, in which npm Inc. states that there has been no security impact. A full post-mortem is said to be forthcoming, and will be added here when it's published.

__UPDATE 3:__ The [full postmortem](http://blog.npmjs.org/post/169582189317/incident-report-npm-inc-operations-incident-of) has been published, including a list of affected packages.

---

There's currently a [known issue](https://status.npmjs.org/incidents/41zfb8qpvrdj) affecting the NPM registry, making certain packages impossible to install. You may see an error message indicating that a package doesn't exist, or that the registry is down. More details can be found [here](https://github.com/npm/registry/issues/255).

## What are the consequences?

1. You will not be able to install the missing packages.
2. ~~If you've installed any of the missing packages during the incident, __it is possible that you may have installed malware__. There are no confirmed cases of this yet, but there are reports that it's possible for third parties to re-register missing package names, which would theoretically allow them to install malware.~~

## What should I do now?

* Do not install __any__ packages until the issue has been resolved.
* ~~If you've installed packages today, please go through them and verify as best as you can that there are no malicious postinstall scripts in them.~~
* If you're paying for private NPM hosting, please wait for NPM to resolve the issue, and afterwards contact NPM support to ask them for a post-mortem describing the impact, its cause, and whether any malicious packages were uploaded.
* ~~Rotate (ie. replace/regenerate) any credentials (passwords, keypairs, certificates, ...) on systems where you've installed NPM packages today during the incident.~~
* Consider setting up your own 'registry proxy' using [verdaccio](https://github.com/verdaccio/verdaccio), so that you won't be affected by issues like this in the future.